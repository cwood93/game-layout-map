# Game layout map

[Will be updating the game design document to reflect these changes]

[work in progress -- design is likely to change while setting will likely stay]

  The player(s) will be starting in the center of the map, while the enemies
(zombies) spawn from a nearby site (and later, corners of the map) in hopes to
motivate the player to choose a nearby spot of interest and explore it whilst 
evading the enemies.  Weapons and pickups will be hidden mainly on the side of
or very close to buildings.  Main areas of interest will likely contain the most
interesting weapons/utilities.

  Ideally, killing an enemy will give money which allows for purchasing weapons
and pickups from their given locations.  All buildings should be accessable by
the player and possibly contain static "traps" that can be activated through
currency/basic interaction and help temporarily protect the player from being 
killed.  While there is no current scenario for "winning" and ending the game
simultaneously, there may be future plans to incorporate a boat at the harbor
area in the southeast portion of the map that will have a large purchase "barrier"
that will cause the game to end and the player to win.  The following is highly
unlikely, but if there is still more time then perhaps the boat will begin out
as broken and the player will have to find parts in the town to make repairs then
purchase the "ending".